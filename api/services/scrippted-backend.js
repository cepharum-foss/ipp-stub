const File = require( "node:fs/promises" );
const Path = require( "node:path" );

const { TypeInteger, TypeUri, TypeEnum, TypeKeyword, TypeMimeMediaType } = require( "@cepharum/ipp" );

module.exports = function ( options, OriginalScripptedBackend ) {
	const api = this;
	const { services } = api.runtime;

	const logInfo = api.log( "virtual-ipp:info" );

	let latestJobId = 1;

	const spoolFolderName = options.arguments.folder || process.env.SPOOL_FOLDER || __dirname;

	logInfo( "spool folder is at %s", spoolFolderName );

	class ScripptedBackend extends OriginalScripptedBackend {
		static async printJob( req, res, ipp, requestData ) {
			const printerName = req.params.printer;
			const printerConfig = services.ScripptedPrinterConfiguration.getByName( printerName );

			const jobId = ++latestJobId;

			logInfo( "handle request for printing job #%d to printer %s", jobId, printerName );

			// derive URL describing generated job resource
			const jobUrl = new URL( req.context.url.toString() );
			jobUrl.search = "";
			jobUrl.pathname = jobUrl.pathname.replace( new RegExp( `(/${printerName})(/.*)?$` ), "$1" ) + "/" + jobId;

			// choose filename extension based on job's format
			const format = ipp.attributes.operation.get( "document-format", new TypeMimeMediaType( printerConfig.mimes[0] ) ).value;
			let ext;

			if ( /[/-]pdf$/.test( format ) ) {
				ext = "pdf";
			} else if ( /[/-]postscript$/.test( format ) ) {
				ext = "ps";
			} else {
				ext = "prn";
			}

			// write all print job data to a local file
			const spoolFileName = Path.resolve( spoolFolderName, `${jobId}.${ext}` );

			logInfo( "writing job to file %s", spoolFileName );

			await File.writeFile( spoolFileName, requestData )
				.catch( error => {
					throw new services.HttpException( 500, `writing to file failed: ${error.message}` )
				} );

			// send response on successful "print"
			return ipp.deriveResponse()
				.setJobAttribute( "job-id", new TypeInteger( jobId ) )
				.setJobAttribute( "job-uri", new TypeUri( jobUrl.toString() ) )
				.setJobAttribute( "job-state", new TypeEnum( 9 ) ) // completed -> make it vanish from client's queue
				.setJobAttribute( "job-state-reason", new TypeKeyword( "none" ) );
		}
	}

	return ScripptedBackend;
};
