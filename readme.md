# Virtual IPP printer

## Prerequisites

* NodeJS v16+

## Install

Install project's dependencies with command:

```bash
npm install
```

## Run the tool

Invoke command

```bash
npm start
```

## Setup printer

1. Open Windows printers configuration.
2. Add new printer.
3. Wait for printer discovery to offer "manual setup" and click it.
4. Create network printer with URL http://localhost:631/printers/foo-pdf
   * `localhost` must select the host this tool is running on
   * `631` is regular IPP port. Using different port is possible but requires different invocation of this tool, too.
   * `printers` is a mandatory segment.
   * `foo` is an arbitrary name which is basically ignored by this tool.
   * `pdf` is a supported printer name suffix causing this tool to instruct clients like Windows to use a PDF printer driver.
5. Provide a name for the printer.
6. Try printing a test page. It should appear as PDF document in root folder of this project.
